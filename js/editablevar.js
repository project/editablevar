(function($) {
    Drupal.behaviors.editablevar = {
        attach: function(context, settings) {
            $('table.js-editablevars > tbody > tr').each(function() {
                var targetTD = $(this).find("td:eq(1)");
                var targetTXT = targetTD.text();
                var isMatch = (targetTXT.length == 7 && targetTXT.substr(0, 1) == "#") || targetTXT === 'white' || targetTXT === 'black';
                if (isMatch) {
                    targetTD.css({"border-left-color": targetTXT, 
                                  "border-left-width":"40px", 
                                  "border-left-style":"solid"});
                } else {
                    targetTD.css({"padding-left":"30px"});
                }
            });
        }
    };
})( jQuery );