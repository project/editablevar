<?php

/**
 * @file
 * Contains \Drupal\editablevar\EditablevarFinalizeForm.
 */

namespace Drupal\editablevar;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Cache\Cache;  // to clear the cache
use Drupal\Core\PhpStorage\PhpStorageFactory;  // to clear the cache

/**
 * Builds the form to delete a forum term.
 */
class EditablevarFinalizeForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'editablevar_finalize_form';
  }
  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('The CSS/JSON files will be created and the cache will be cleared now.');
  }
 
  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Finalize variable changes?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('editablevar.group_list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Finalize');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->finalizeGenerateFiles();
    $this->finalizeCacheClear();
    \Drupal::messenger()->addStatus($this->t('Finished finalizing variable changes.'));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

  /**
   * Generate CSS and JSON files
   */
  public function finalizeGenerateFiles() {
    foreach ($groups = EditablevarGroupStorage::load() as $group) {
      if ($group->source_file!='') {
        if ($group->target_file=='') {
          \Drupal::messenger()->addError($this->t('Group @group: target file is not set', array('@group' => $group->name)));
          continue;
        }
        if (!file_exists($group->source_file)) {
          \Drupal::messenger()->addError($this->t('Group @group: source file is missing: @file', array('@group' => $group->name, '@file' => $group->source_file)));
          continue;
        }
        $content = file_get_contents($group->source_file);
        if ($content===FALSE) {
          \Drupal::messenger()->addError($this->t('Group @group: failed to load source file : @file', array('@group' => $group->name, '@file' => $group->source_file)));
          continue;
        }
        // collect variables of this group and its values
        $var_values = array();
        foreach ($vars = EditablevarVarStorage::load(array('group_record_id' => $group->record_id)) as $var) {
          $var_values[$var->id] = $var->value;
        }
        if ($var_values) {
          // replace patterns in a reverse order, this helps to process longer variables first.
          // (to process 'example_longer' before 'example')
          krsort($var_values);
          foreach ($var_values as $id => $value) {
            $content = str_replace('$' . $id, $value, $content);
          }
        }
        // write the target file
        if (file_put_contents($group->target_file, $content)!==FALSE) {
          \Drupal::messenger()->addStatus($this->t('Group @group: generated target file: @file', array('@group' => $group->name, '@file' => $group->target_file)));
        }
        else {
          \Drupal::messenger()->addError($this->t('Group @group: failed to write target file: @file', array('@group' => $group->name, '@file' => $group->target_file)));
        }
      }
      if ($group->json_file!='') {
        // collect variables of this group and its values
        $var_values = array();
        foreach ($vars = EditablevarVarStorage::load(array('group_record_id' => $group->record_id)) as $var) {
          $var_values[$var->id] = $var->value;
        }
        ksort($var_values);
        // convert to JSON format
        $content = json_encode($var_values);
        // write the target file
        if (file_put_contents($group->json_file, $content)!==FALSE) {
          \Drupal::messenger()->addStatus($this->t('Group @group: generated jSON file: @file', array('@group' => $group->name, '@file' =>$group->json_file)));
        }
        else {
          \Drupal::messenger()->addError($this->t('Group @group: failed to write JSON file: @file', array('@group' => $group->name, '@file' => $group->json_file)));
        }
      }
    }
  }
    
  
  /**
   * Clear the cache, but only partially
   */
  public function finalizeCacheClear() {
    // Clear the cache: simplified version of drupal_flush_all_caches()
    $module_handler = \Drupal::moduleHandler();
    // Flush all persistent caches.
    // This is executed based on old/previously known information, which is
    // sufficient, since new extensions cannot have any primed caches yet.
    $module_handler->invokeAll('cache_flush');
    foreach (Cache::getBins() as $service_id => $cache_backend) {
      if (in_array($service_id, array('render', 'dynamic_page_cache', 'data'))) {
        $cache_backend->deleteAll();
      }
    }

    // Flush asset file caches.
    if (TRUE) {
      \Drupal::service('asset.css.collection_optimizer')->deleteAll();
      \Drupal::service('asset.js.collection_optimizer')->deleteAll();
      _drupal_flush_css_js();

      // Reset all static caches.
      drupal_static_reset();


      if (!$kernel instanceof DrupalKernel) {
        $kernel = \Drupal::service('kernel');
        $kernel->invalidateContainer();
        $kernel->rebuildContainer();
      }
    }

    // Wipe the Twig PHP Storage cache.
    \Drupal::service('twig')->invalidate();
 

    if (FALSE) {
      // Rebuild theme data that is stored in state.
      \Drupal::service('theme_handler')->refreshInfo();
      // In case the active theme gets requested later in the same request we need
      // to reset the theme manager.
      \Drupal::theme()->resetActiveTheme();
    }

    if (FALSE) {
      // Rebuild and reboot a new kernel. A simple DrupalKernel reboot is not
      // sufficient, since the list of enabled modules might have been adjusted
      // above due to changed code.
      $files = array();
      foreach ($module_data as $name => $extension) {
        if ($extension->status) {
          $files[$name] = $extension;
        }
      }
      \Drupal::service('kernel')->updateModules($module_handler->getModuleList(), $files);
      // New container, new module handler.
      $module_handler = \Drupal::moduleHandler();

      // Ensure that all modules that are currently supposed to be enabled are
      // actually loaded.
      $module_handler->loadAll();

      // Rebuild all information based on new module data.
      $module_handler->invokeAll('rebuild');

      // Clear all plugin caches.
      \Drupal::service('plugin.cache_clearer')->clearCachedDefinitions();

      // Rebuild the menu router based on all rebuilt data.
      // Important: This rebuild must happen last, so the menu router is guaranteed
      // to be based on up to date information.
      \Drupal::service('router.builder')->rebuild();

      // Re-initialize the maintenance theme, if the current request attempted to
      // use it. Unlike regular usages of this function, the installer and update
      // scripts need to flush all caches during GET requests/page building.
      if (function_exists('_drupal_maintenance_theme')) {
        \Drupal::theme()->resetActiveTheme();
        drupal_maintenance_theme();
      }
    }
  }

}
