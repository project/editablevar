<?php

/**
 * @file
 * Contains \Drupal\editablevar\EditablevarGroupAddForm
 */

namespace Drupal\editablevar;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Simple form to add an entry, with all the interesting fields.
 */
class EditablevarGroupAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'editablevar_group_add_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = array();

    $form['add'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add new group'),
    );
    $form['add']['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Group Name'),
      '#size' => 30,
      '#required' => TRUE,
    );
    $form['add']['id'] = array(
      '#type' => 'machine_name',
      '#maxlength' => 64,
      '#description' => $this->t('A unique name for this group. Must be alpha-numeric and underscore separated.'),
      '#default_value' => '',
      '#machine_name' => array(
        'exists' => array($this, 'exists'),
        'replace_pattern' => '[^a-z0-9_.]+',
        'source' => array('add', 'name'),
      ),
      '#required' => TRUE,
      '#disabled' => FALSE,
    );
    $form['add']['description'] = array(
      '#type' => 'textarea',
      '#title' => t('Description'),
    );
    $form['files'] = array(
      '#type' => 'fieldset',
      '#title' => t('Generated files'),
    );
    $form['files']['note'] = array(
      '#markup' => '<div>' . t('Note: The file path must start with "themes/" or "sites/default/files/".') . '</div>'
    );
    $form['files']['source_file'] = array(
      '#type' => 'textfield',
      '#title' => t('Source File (template)'),
      '#size' => 30,
      '#description' => t('Optional. A file path relative to site path. If set, this file will be used as a template to generate the target file. The $variablename in the source file will be replaced with the actual value. For example, "$example" in source file will become "12345" in the target file.'),
    );
    $form['files']['target_file'] = array(
      '#type' => 'textfield',
      '#title' => t('Target File (created)'),
      '#size' => 30,
      '#description' => t('Optional. A file path relative to site path. If set, this file will be created or overwritten based on the template and the variables.'),
    );
    $form['files']['json_file'] = array(
      '#type' => 'textfield',
      '#title' => t('JSON File (created)'),
      '#size' => 30,
      '#description' => t('Optional. A file path relative to site path. If set, this file will be created or overwritten with the values of the variables.'),
    );
    $form['vars'] = array(
      '#type' => 'fieldset',
      '#title' => t('Variables'),
    );
    $form['vars']['var_id_prefix'] = array(
      '#type' => 'textfield',
      '#maxlength' => 64,
      '#title' => $this->t('Variable name prefix'),
      '#description' => $this->t('Optional. A prefix used for all variable names in this group. Must be alpha-numeric and underscore separated.'),
      '#default_value' => '',
      '#required' => FALSE,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add group'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach (array('source_file', 'target_file', 'json_file') as $file) {
      $value = trim($form_state->getValue($file));
      if ($value!='') {
        if (preg_match('/[^0-9a-zA-Z-_\.\/]/', $value)) {
          $form_state->setErrorByName($file, $this->t('Special characters are not allowed'));
        }
        elseif (!preg_match('!^themes/.+!', $value) && !preg_match('!^sites/default/files/.+!', $value))  {
          $form_state->setErrorByName($file, $this->t('Invalid directory'));
        }
        elseif (preg_match('!/$!', $value)) {
          $form_state->setErrorByName($file, $this->t('Please enter a filename, not a directory.'));
        }
      }
    }
    $var_id_prefix = $form_state->getValue('var_id_prefix');
    if ($var_id_prefix!='') {
      if (preg_match('@[^a-z0-9_]+@', $var_id_prefix)) {
        $form_state->setError($element, t('The machine-readable name must be alpha-numeric and underscore separated.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the submitted entry.
    $entry = array(
      'id' => $form_state->getValue('id'),
      'name' => trim($form_state->getValue('name')),
      'description' => trim($form_state->getValue('description')),
      'source_file' => trim($form_state->getValue('source_file')),
      'target_file' => trim($form_state->getValue('target_file')),
      'json_file' => trim($form_state->getValue('json_file')),
      'var_id_prefix' => trim($form_state->getValue('var_id_prefix')),
    );
    $return = EditablevarGroupStorage::insert($entry);
    if ($return) {
      \Drupal::messenger()->addStatus(t('Created group @group', array('@group' => $form_state->getValue('name'))));
      $form_state->setRedirectUrl(new Url('editablevar.var_list', array('editablevar_group_id' => $form_state->getValue('id'))));
    }
  }
  public function exists($id, array $element, FormStateInterface $form_state) {
    $database = \Drupal::database();
    // Read entry with the given ID
    $select = $database->select('editablevar_groups', 'groups');
    $select->fields('groups');
    $select->condition('id', $id);

    // Return the result as boolean
    return (bool) $select->execute()->fetchAll();
  }
}
