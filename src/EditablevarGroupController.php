<?php

/**
 * @file
 * Contains \Drupal\editablevar\EditablevarGroupController.
 */

namespace Drupal\editablevar;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Controller for Editablevar Groups
 */
class EditablevarGroupController extends ControllerBase {

  /**
   * Render a list of entries in the database.
   */
  public function entryList() {
    $content = array();

    $rows = array();
    $headers = array(t('Variable group'), t('Operations'));

    foreach ($entries = EditablevarGroupStorage::load() as $entry) {
      $list_url = Url::fromRoute('editablevar.var_list', array('editablevar_group_id' => $entry->id));
      $add_url = Url::fromRoute('editablevar.var_add', array('editablevar_group_id' => $entry->id));
      $edit_url = Url::fromRoute('editablevar.group_update', array('editablevar_group_id' => $entry->id));
      $delete_url = Url::fromRoute('editablevar.group_delete', array('editablevar_group_id' => $entry->id));
      $lines1 = array();
      $lines2 = array();
      $lines1[]= '<b>' . Link::fromTextAndUrl(\Drupal\Component\Utility\Html::escape($entry->name), $list_url)->toString() . '</b>';
      if ($entry->description!='') {
        $lines1[]= \Drupal\Component\Utility\Html::escape($entry->description);
      }
      if ($entry->source_file!='') {
        $lines1[]= t('Source file: @file', array('@file' => $entry->source_file));
      }
      if ($entry->target_file!='') {
        $lines1[]= t('Target file: @file', array('@file' => $entry->target_file));
      }
      if ($entry->json_file!='') {
        $lines1[]= t('JSON file: @file', array('@file' => $entry->json_file));
      }
      $variable_count = 0;
      foreach ($variables=EditablevarVarStorage::load(array('group_record_id' => $entry->record_id)) as $variable) {
        $variable_count++;
      }
      $lines2[]=Link::fromTextAndUrl(t('View variables (@count)', array('@count' => $variable_count)), $list_url)->toString();
      $lines2[]=Link::fromTextAndUrl(t('Add variables'), $add_url)->toString();
      $lines2[]='&nbsp;';
      $lines2[]=Link::fromTextAndUrl(t('Edit group'), $edit_url)->toString();
      $lines2[]=Link::fromTextAndUrl(t('Delete group'), $delete_url)->toString();
      $lines2[]='&nbsp;';
      $rows[$entry->id] = array(
        array('data' => array('#markup' => implode('<br>', $lines1))),
        array('data' => array('#markup' => implode('<br>', $lines2))),
      );
    }
    ksort($rows);
    $content['table'] = array(
      '#type' => 'table',
      '#attributes' => [
        'class' => ['table-editablevars'],
      ],
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => t('No groups available.'),
    );
    // Don't cache this page.
    $content['#cache']['max-age'] = 0;

    return $content;
  }

}
