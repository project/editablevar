<?php

/**
 * @file
 * Contains \Drupal\editablevar\EditablevarGroupEditablevarGroupDeleteForm.
 */

namespace Drupal\editablevar;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Builds the form to delete a forum term.
 */
class EditablevarGroupDeleteForm extends ConfirmFormBase {

  /**
   * The group being deleted.
   */
  protected $group_record_id;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'editablevar_group_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    foreach($groups = EditablevarGroupStorage::load(array('record_id' => $this->group_record_id)) as $group) {
      return $this->t('Are you sure you want to delete the group %label and all its variables?', array('%label' => $group->name));
    }
    return $this->t('Failed to load this group');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('editablevar.group_list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $editablevar_group_id = NULL) {
    foreach ($groups = EditablevarGroupStorage::load(array('id' => $editablevar_group_id)) as $group) {;
      $this->group_record_id = $group->record_id;
      return parent::buildForm($form, $form_state);
    }
    return array('#markup' => $this->t('Failed to load this group'));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($groups = EditablevarGroupStorage::load(array('record_id' => $this->group_record_id)) as $group) {
      $count = 0;
      foreach ($variables=EditablevarVarStorage::load(array('group_record_id' => $this->group_record_id)) as $variable) {
        EditablevarVarStorage::delete((array)$variable);
        $count++;
      }
      EditablevarGroupStorage::delete((array)$group);
      \Drupal::messenger()->addStatus($this->t('The group %label and all its %count variables have been deleted.', array('%label' => $group->name, '%count' => $count)));
      $form_state->setRedirectUrl($this->getCancelUrl());
      return;
    }
    \Drupal::messenger()->addError($this->t('Failed to delete this group'));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
