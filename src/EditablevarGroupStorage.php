<?php

/**
 * @file
 * Contains \Drupal\editablevar\EditablevarGroupStorage
 */

namespace Drupal\editablevar;

class EditablevarGroupStorage {

  /**
   * Save an entry in the database.
   *
   * @param array $entry
   *   An array containing all the fields of the database record.
   *
   * @return int
   *   The number of updated rows.
   *
   * @throws \Exception
   *   When the database insert fails.
   */
  public static function insert($entry) {
    $return_value = NULL;
    $database = \Drupal::database();
    try {
      $return_value = $database->insert('editablevar_groups')
          ->fields($entry)
          ->execute();
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError(t('insert failed. Message = %message, query= %query', array(
            '%message' => $e->getMessage(),
            '%query' => $e->query_string,
          )));
    }
    return $return_value;
  }

  /**
   * Update an entry in the database.
   *
   * @param array $entry
   *   An array containing all the fields of the item to be updated.
   *
   * @return int
   *   The number of updated rows.
   */
  public static function update($entry) {
    $database = \Drupal::database();
    try {
      $count = $database->update('editablevar_groups')
        ->fields($entry)
        ->condition('record_id', $entry['record_id'])
        ->execute();
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError(t('update failed. Message = %message, query= %query', array(
            '%message' => $e->getMessage(),
            '%query' => $e->query_string,
          )));
    }
    return $count;
  }

  /**
   * Delete an entry from the database.
   *
   * @param array $entry
   *   An array containing at least the person identifier 'pid' element of the
   *   entry to delete.
   */
  public static function delete($entry) {
    $database = \Drupal::database();
    $database->delete('editablevar_groups')
      ->condition('record_id', $entry['record_id'])
      ->execute();
  }

  /**
   * Read from the database using a filter array.
   */
  public static function load($entry = array()) {
    // Read all fields from the editablevar_groups table.
    $database = \Drupal::database();
    $select = $database->select('editablevar_groups');
    $select->fields('editablevar_groups');

    // Add each field and value as a condition to this query.
    foreach ($entry as $field => $value) {
      $select->condition($field, $value);
    }
    // Return the result in object format.
    return $select->execute()->fetchAll();
  }

}
