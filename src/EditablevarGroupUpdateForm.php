<?php

/**
 * @file
 * Contains \Drupal\editablevar\EditablevarGroupUpdateForm
 */

namespace Drupal\editablevar;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * UI to update a record.
 */
class EditablevarGroupUpdateForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'editablevar_group_update_form';
  }

  /**
   * UI to update a record.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $editablevar_group_id=0) {
    
    $group = NULL;
    foreach ($groups = EditablevarGroupStorage::load(array('id' => $editablevar_group_id)) as $param_group) {
      $group=$param_group;
    }
    if (!$group) {
      return array('#markup' => $this->t('Failed to load this group'));
    }
    $form = array();
    $form['record_id'] = array(
      '#type' => 'value',
      '#value' => $group->record_id,
    );
    $form['group'] = array(
      '#type' => 'fieldset',
      '#title' => t('Variable group'),
    );
    $form['group']['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Group Name'),
      '#size' => 30,
      '#required' => TRUE,
      '#default_value' => $group->name,
    );
    $form['group']['id'] = array(
      '#type' => 'machine_name',
      '#maxlength' => 64,
      '#description' => $this->t('A unique name for this group. Must be alpha-numeric and underscore separated.'),
      '#default_value' => $group->id,
      '#machine_name' => array(
        'exists' => array($this, 'exists'),
        'replace_pattern' => '[^a-z0-9_.]+',
        'source' => array('group', 'name'),
      ),
      '#required' => TRUE,
      '#disabled' => FALSE,
    );
    $form['group']['description'] = array(
      '#type' => 'textarea',
      '#title' => t('Description'),
      '#default_value' => $group->description,
    );
    $form['files'] = array(
      '#type' => 'fieldset',
      '#title' => t('Generated files'),
    );
    $form['files']['note'] = array(
      '#markup' => '<div>' . t('Note: The file path must start with "themes/" or "sites/default/files/".') . '</div>'
    );
    $form['files']['source_file'] = array(
      '#type' => 'textfield',
      '#title' => t('Source File (template)'),
      '#size' => 30,
      '#description' => t('Optional. A file path relative to site path. If set, this file will be used as a template to generate the target file. The $variablename in the source file will be replaced with the actual value. For example, "$example" in source file will become "12345" in the target file.'),
      '#default_value' => $group->source_file,
    );
    $form['files']['target_file'] = array(
      '#type' => 'textfield',
      '#title' => t('Target File (created)'),
      '#size' => 30,
      '#description' => t('Optional. A file path relative to site path. If set, this file will be created or overwritten based on the template and the variables.'),
      '#default_value' => $group->target_file,
    );
    $form['files']['json_file'] = array(
      '#type' => 'textfield',
      '#title' => t('JSON File (created)'),
      '#size' => 30,
      '#description' => t('Optional. A file path relative to site path. If set, this file will be created or overwritten with the values of the variables.'),
      '#default_value' => $group->json_file,
    );
    $form['vars'] = array(
      '#type' => 'fieldset',
      '#title' => t('Variables'),
    );
    $form['vars']['var_id_prefix'] = array(
      '#type' => 'machine_name',
      '#maxlength' => 64,
      '#title' => $this->t('Variable name prefix'),
      '#description' => $this->t('A prefix used for all variable names in this group. Must be alpha-numeric and underscore separated.') . '<br>' . $this->t('If you change this value, existing variables will be updated to have the new prefix.'),
      '#default_value' => $group->var_id_prefix,
      '#machine_name' => array(
        'replace_pattern' => '[^a-z0-9_.]+',
      ),
      '#required' => FALSE,
    );
    $form['vars']['var_id_prefix_original'] = array(
      '#type' => 'value',
      '#value' => $group->var_id_prefix
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update group'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach (array('source_file', 'target_file', 'json_file') as $file) {
      $value = trim($form_state->getValue($file));
      if ($value!='') {
        if (preg_match('/[^0-9a-zA-Z-_\.\/]/', $value)) {
          $form_state->setErrorByName($file, $this->t('Special characters are not allowed'));
        }
        elseif (!preg_match('!^themes/.+!', $value) && !preg_match('!^sites/default/files/.+!', $value))  {
          $form_state->setErrorByName($file, $this->t('Invalid directory'));
        }
        elseif (preg_match('!/$!', $value)) {
          $form_state->setErrorByName($file, $this->t('Please enter a filename, not a directory.'));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $database = \Drupal::database();
    // Check whether var_id_prefix has changed
    if ($form_state->getValue('var_id_prefix')!=$form_state->getValue('var_id_prefix_original')) {
      $new=$form_state->getValue('var_id_prefix');
      $old=$form_state->getValue('var_id_prefix_original');
      \Drupal::messenger()->addStatus($this->t('Changing variable prefix from "@old" to "@new"', array('@old' => $old, '@new' => $new)));
      foreach ($vars = EditablevarVarStorage::load(array('group_record_id' => $form_state->getValue('record_id'))) as $var) {
        // remove old prefix
        $id = $var->id;
        if (($old!='') && (substr($id, 0, strlen($old))==$old)) {
          $id = substr($id, strlen($old));
        }
        // add new prefix
        $id = $new . $id;
        // check whether such a variable exists
        $select = $database->select('editablevar_vars');
        $select->fields('editablevar_vars');
        $select->condition('id', $id);
        if ($select->execute()->fetchAll()) {
          \Drupal::messenger()->addError($this->t('ERROR: Could not rename variable from "@old" to "@new", that variable already exists.', array('@old' => $var->id, '@new' => $id)));
        }
        else {
          $old_id = $var->id;
          $var->id = $id;
          $return = EditablevarVarStorage::update((array)$var);
          if ($return) {
            \Drupal::messenger()->addStatus($this->t('Renamed variable from "@old" to "@new".', array('@old' => $old_id, '@new' => $id)));
          }
          else {
            \Drupal::messenger()->addError($this->t('ERROR: failed to rename variable from "@old" to "@new".', array('@old' => $old_id, '@new' => $id)));
          }
        }
      }
    }
    // Save the submitted entry.
    $entry = array(
      'record_id' => $form_state->getValue('record_id'),
      'id' => $form_state->getValue('id'),
      'name' => trim($form_state->getValue('name')),
      'var_id_prefix' => $form_state->getValue('var_id_prefix'),
      'description' => trim($form_state->getValue('description')),
      'source_file' => trim($form_state->getValue('source_file')),
      'target_file' => trim($form_state->getValue('target_file')),
      'json_file' => trim($form_state->getValue('json_file')),
    );
    $return = EditablevarGroupStorage::update($entry);
    if ($return) {
      \Drupal::messenger()->addStatus(t('Updated group @group', array('@group' => $form_state->getValue('name'))));
      $form_state->setRedirectUrl(new Url('editablevar.group_list'));
    }
  }
  /**
   * Helper function for #machine_name
   */
  public function exists($id, array $element, FormStateInterface $form_state) {
    $database = \Drupal::database();
    // Read entry with the given ID
    $select = $database->select('editablevar_groups', 'groups');
    $select->fields('groups');
    $select->condition('id', $id);

    // Return the result as boolean
    return (bool) $select->execute()->fetchAll();
  }
}
