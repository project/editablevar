<?php

/**
 * @file
 * Contains \Drupal\editablevar\EditablevarVarAddForm
 */

namespace Drupal\editablevar;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Simple form to add an entry, with all the interesting fields.
 */
class EditablevarVarAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'editablevar_var_add_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $editablevar_group_id = 0) {
    $group = NULL;
    foreach ($groups = EditablevarGroupStorage::load(array('id' => $editablevar_group_id)) as $param_group) {
      $group=$param_group;
    }
    if (!$group) {
      return array('#markup' => $this->t('Failed to load this group'));
    }
    $form = array();
    $form['var_id_prefix'] = array(
      '#type' => 'value',
      '#value' => $group->var_id_prefix,
    );

    $form['group_record_id'] = array(
      '#type' => 'value',
      '#value' => $group->record_id,
    );

    $form['add'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add new variable'),
    );
    $form['add']['id'] = array(
      '#type' => 'machine_name',
      '#title' => $this->t('Name'),
      '#maxlength' => 64,
      '#description' => $this->t('A unique name for this variable. Must be alpha-numeric and underscore separated.'),
      '#default_value' => '',
      '#field_prefix' => $group->var_id_prefix,
      '#machine_name' => array(
        'exists' => array($this, 'exists'),
      ),
      '#required' => TRUE,
      '#disabled' => FALSE,
    );
    $form['add']['description'] = array(
      '#type' => 'textarea',
      '#title' => t('Description'),
    );
    $form['add']['value'] = array(
      '#type' => 'textarea',
      '#title' => t('Value'),
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add variable'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $id = $form_state->getValue('var_id_prefix') . $form_state->getValue('id');
    if (strlen($id)>=100) {
      $form_state->setErrorByName('id', $this->t('Variable name must be less than 100 characters long.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the submitted entry.
    $id = $form_state->getValue('var_id_prefix') . $form_state->getValue('id');
    $entry = array(
      'id' => $id,
      'group_record_id' => $form_state->getValue('group_record_id'),
      'description' => trim($form_state->getValue('description')),
      'value' => trim($form_state->getValue('value')),
    );
    $return = EditablevarVarStorage::insert($entry);
    if ($return) {
      \Drupal::messenger()->addStatus(t('Created variable @variable', array('@variable' => $id)));
    }
  }
  public function exists($id, array $element, FormStateInterface $form_state) {
    $database = \Drupal::database();
    // Read entry with the given ID
    $select = $database->select('editablevar_vars', 'vars');
    $select->fields('vars');
    $select->condition('id', $element['#field_prefix'] . $id);
    // Return the result as boolean
    $ret = $select->execute()->fetchAll();
    return (bool) $ret;
  }
}
