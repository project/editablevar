<?php

/**
 * @file
 * Contains \Drupal\editablevar\EditablevarVarController.
 */

namespace Drupal\editablevar;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Controller for Editablevar
 */
class EditablevarVarController extends ControllerBase {

  /**
   * Render a list of entries in the database.
   */
  public function entryList($editablevar_group_id = NULL) {
    // load and validate group
    $group = NULL;
    foreach ($groups = EditablevarGroupStorage::load(array('id' => $editablevar_group_id)) as $param_group) {
      $group=$param_group;
    }
    if (!$group) {
      return array('#markup' => $this->t('Failed to load this group'));
    }
    $content = array();

    $rows = array();
    $headers = array(t('Name'), t('Value'), t('Description'));

    foreach ($entries = EditablevarVarStorage::load(array('group_record_id' => $group->record_id)) as $entry) {
      $edit_url = Url::fromRoute('editablevar.var_update', array('editablevar_var_id' => $entry->id));
      $edit_link = Link::fromTextAndUrl(\Drupal\Component\Utility\Html::escape($entry->id), $edit_url)->toString();
      $name= '<b>' . $edit_link . '</b>';
      $value= \Drupal\Component\Utility\Html::escape($entry->value);
      $description= \Drupal\Component\Utility\Html::escape($entry->description);
      $rows[$entry->id] = array(
        array('data' => array('#markup' => $name)),
        array('data' => array('#markup' => $value)),
        array('data' => array('#markup' => $description)),
      );
    }
    ksort($rows);
    $content['table'] = array(
      '#type' => 'table',
      '#attributes' => [
        'class' => ['table-editablevars','js-editablevars'],
      ],
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => t('No variables available.'),
    );
    // Don't cache this page.
    $content['#cache']['max-age'] = 0;

    return $content;
  }

}
