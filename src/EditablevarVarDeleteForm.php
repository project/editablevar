<?php

/**
 * @file
 * Contains \Drupal\editablevar\EditablevarVarEditablevarVarDeleteForm.
 */

namespace Drupal\editablevar;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Builds the form to delete a forum term.
 */
class EditablevarVarDeleteForm extends ConfirmFormBase {

  /**
   * The var being deleted.
   */
  protected $var_record_id;
  protected $var_id;

  /**
   * The group id of the this var
   */
  protected $group_id;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'editablevar_var_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the variable %label?', array('%label' => $this->var_id));
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('editablevar.var_list', array('editablevar_group_id' => $this->group_id));
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $editablevar_var_id = NULL) {
    foreach ($vars = EditablevarVarStorage::load(array('id' => $editablevar_var_id)) as $var) {;
      $this->var_record_id = $var->record_id;
      $this->var_id = $var->id;
      foreach ($groups = EditablevarGroupStorage::load(array('record_id' => $var->group_record_id)) as $group) {
        $this->group_id = $group->id;
        return parent::buildForm($form, $form_state);
      }
      return array('#markup' => $this->t('Failed to load the group of this var'));
    }
    return array('#markup' => $this->t('Failed to load this var'));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($vars = EditablevarVarStorage::load(array('record_id' => $this->var_record_id)) as $var) {
      EditablevarVarStorage::delete((array)$var);
      \Drupal::messenger()->addStatus($this->t('The variable %label has been deleted.', array('%label' => $var->id)));
      $form_state->setRedirectUrl($this->getCancelUrl());
      return;
    }
    \Drupal::messenger()->addError($this->t('Failed to delete this variable'));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
