<?php

/**
 * @file
 * Contains \Drupal\editablevar\EditablevarVarStorage
 */

namespace Drupal\editablevar;

class EditablevarVarStorage {

  /**
   * Save an entry in the database.
   *
   * Exception handling is shown in this example. It could be simplified
   * without the try/catch blocks, but since an insert will throw an exception
   * and terminate your application if the exception is not handled, it is best
   * to employ try/catch.
   *
   * @param array $entry
   *   An array containing all the fields of the database record.
   *
   * @return int
   *   The number of updated rows.
   *
   * @throws \Exception
   *   When the database insert fails.
   */
  public static function insert($entry) {
    $return_value = NULL;
    $database = \Drupal::database();
    try {
      $return_value = $database->insert('editablevar_vars')
        ->fields($entry)
        ->execute();
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError(t('insert failed. Message = %message, query= %query', array(
            '%message' => $e->getMessage(),
            '%query' => $e->query_string,
          )));
    }
    return $return_value;
  }

  /**
   * Update an entry in the database.
   *
   * @param array $entry
   *   An array containing all the fields of the item to be updated.
   *
   * @return int
   *   The number of updated rows.
   */
  public static function update($entry) {
    $database = \Drupal::database();
    try {
      $count = $database->update('editablevar_vars')
          ->fields($entry)
          ->condition('record_id', $entry['record_id'])
          ->execute();
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError(t('update failed. Message = %message, query= %query', array(
            '%message' => $e->getMessage(),
            '%query' => $e->query_string,
          )));
    }
    return $count;
  }

  /**
   * Delete an entry from the database.
   *
   * @param array $entry
   *   An array containing at least the person identifier 'record_id' element of the
   *   entry to delete.
   */
  public static function delete($entry) {
    $database = \Drupal::database();
    $database->delete('editablevar_vars')
        ->condition('record_id', $entry['record_id'])
        ->execute();
  }

  /**
   * Read from the database using a filter array.
   */
  public static function load($entry = array()) {
    $database = \Drupal::database();
    // Read all fields from the editablevar table.
    $select = $database->select('editablevar_vars');
    $select->fields('editablevar_vars');

    // Add each field and value as a condition to this query.
    foreach ($entry as $field => $value) {
      $select->condition($field, $value);
    }
    // Return the result in object format.
    return $select->execute()->fetchAll();
  }

}
