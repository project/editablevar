<?php

/**
 * @file
 * Contains \Drupal\editablevar\EditablevarVarUpdateForm
 */

namespace Drupal\editablevar;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Simple form to add an entry, with all the interesting fields.
 */
class EditablevarVarUpdateForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'editablevar_var_update_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $editablevar_var_id = 0) {
    $var = NULL;
    foreach ($vars = EditablevarVarStorage::load(array('id' => $editablevar_var_id)) as $param_var) {
      $var=$param_var;
    }
    if (!$var) {
      return array('#markup' => $this->t('Failed to load this variable'));
    }
    $group = NULL;
    foreach ($groups = EditablevarGroupStorage::load(array('record_id' => $var->group_record_id)) as $param_group) {
      $group=$param_group;
    }
    if (!$group) {
      return array('#markup' => $this->t('Failed to load this group'));
    }
    $form = array();
    $form['var_record_id'] = array(
      '#type' => 'value',
      '#value' => $var->record_id,
    );
    $form['var_id_for_delete'] = array(
      '#type' => 'value',
      '#value' => $var->id,
    );
    $form['var_id_prefix'] = array(
      '#type' => 'value',
      '#value' => $group->var_id_prefix,
    );

    $form['group_record_id'] = array(
      '#type' => 'value',
      '#value' => $var->group_record_id,
    );

    $form['add'] = array(
      '#type' => 'fieldset',
      '#title' => t('Update variable'),
    );
    $id_without_prefix = $var->id;
    if ($group->var_id_prefix!='') {
      if (substr($var->id, 0, strlen($group->var_id_prefix))==$group->var_id_prefix) {
        $id_without_prefix = substr($var->id, strlen($group->var_id_prefix));
      }
      else {
        $form['add']['id_prefix_message'] = array(
          '#markup' => '<div class="messages">' . $this->t('The group prefix "@prefix" will be added to the name of this variable when the variable is saved.', array('@prefix' => $group->var_id_prefix)) . '</div>',
        );
      }
    }
    $form['add']['id'] = array(
      '#type' => 'machine_name',
      '#title' => $this->t('Name'),
      '#maxlength' => 64,
      '#description' => $this->t('A unique name for this variable. Must be alpha-numeric and underscore separated.'),
      '#default_value' => $id_without_prefix,
      '#field_prefix' => $group->var_id_prefix,
      '#machine_name' => array(
        'exists' => array($this, 'exists'),
      ),
      '#required' => TRUE,
      '#disabled' => FALSE,
    );
    $form['add']['description'] = array(
      '#type' => 'textarea',
      '#title' => t('Description'),
      '#default_value' => $var->description,
    );
    $form['add']['value'] = array(
      '#type' => 'textarea',
      '#title' => t('Value'),
      '#default_value' => $var->value,
    );
    $form['actions'] = array(
      '#type' => 'actions',
      '#attributes' => array('class' => array('container-inline')),
    );
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    );
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Delete'),
      '#prefix' => '&nbsp;&nbsp;&nbsp;',
      '#submit' => array('::deleteForm'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $id = $form_state->getValue('var_id_prefix') . $form_state->getValue('id');
    if (strlen($id)>=100) {
      $form_state->setErrorByName('id', $this->t('Variable name must be less than 100 characters long.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the submitted entry.
    $id = $form_state->getValue('var_id_prefix') . $form_state->getValue('id');
    $entry = array(
      'record_id' => $form_state->getValue('var_record_id'),
      'id' => $id,
      'description' => trim($form_state->getValue('description')),
      'value' => trim($form_state->getValue('value')),
    );
    $return = EditablevarVarStorage::update($entry);
    if ($return) {
      \Drupal::messenger()->addStatus(t('Updated variable @variable', array('@variable' => $id)));
      foreach ($groups = EditablevarGroupStorage::load(array('record_id' => $form_state->getValue('group_record_id'))) as $group) {
        $form_state->setRedirectUrl(new Url('editablevar.var_list', array('editablevar_group_id' => $group->id)));
      }
    }
  }
  /**
   * {@inheritdoc}
   */
  public function deleteForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirectUrl(new Url('editablevar.var_delete', array('editablevar_var_id' => $form_state->getValue('var_id_for_delete'))));
  }
  public function exists($id, array $element, FormStateInterface $form_state) {
    $database = \Drupal::database();
    // Read entry with the given ID
    $select = $database->select('editablevar_vars', 'vars');
    $select->fields('vars');
    $select->condition('id', $element['#field_prefix'] . $id);
    // Return the result as boolean
    return (bool) $select->execute()->fetchAll();
  }
}
