<?php

/**
 * @file
 * Contains \Drupal\editablevar\Plugin\Filter\FilterEditablevar.
 */

namespace Drupal\editablevar\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to include editablevars into content
 *
 * @Filter(
 *   id = "filter_editablevar",
 *   title = @Translation("Embed editablevar values"),
 *   description = @Translation("Embed editablevar values"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE
 * )
 */
class FilterEditablevar extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);

    if (preg_match_all('/\[editablevar\:([a-z0-9_]+)\]/', $text, $matches)) {
      
      $ids=array_unique($matches[1]);
      foreach ($ids as $id) {
        $value=editablevar_get_value($id);
        if (trim($value)!='') {
          $text=str_replace("[editablevar:$id]", $value, $text);
        }
      }
      $result->setProcessedText($text);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    if ($long) {
      return $this->t('
        <p>You can include editablevars using [editablevar:variable_name]</p>');
    }
    else {
      return $this->t('
        <p>You can include editablevars using [editablevar:variable_name]</p>');
    }
  }

}
