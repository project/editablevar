<?php

/**
 * @file
 * Contains \Drupal\editablevar\Twig\EditablevarExtension.
 */

namespace Drupal\editablevar\Twig;

/**
 * Provides the Editablevar function within Twig templates.
 */
class EditablevarExtension extends \Twig_Extension {

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'editablevar';
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return array(
      new \Twig_SimpleFunction('editablevar', array($this, 'editablevar'), array(
        'is_safe' => array('html'),
        'needs_environment' => TRUE,
        'needs_context' => TRUE,
      )),
    );
  }

  /**
   * Provides Editablevar function to Twig templates.
   *
   * Handles 1 or 2 arguments.
   *
   * @param Twig_Environment $env
   *   The twig environment instance.
   * @param array $context
   *   An array of parameters passed to the template.
   */
  public function editablevar(\Twig_Environment $env, array $context) {
    $output = '';

    if (func_num_args() === 2) {
      // No arguments passed to editablevar()
      return $output;
    }

    $args = func_get_args();
    $id = $args[2];
    if (($id=='') || (!is_string($id) && !is_int($id))) {
      return $output;
    }
    // use optional default value, if any given
    if (count($args)>3) {
      $output = $args[3];
    }
    foreach ($vars = \Drupal\editablevar\EditablevarVarStorage::load(array('id' => $id)) as $var) {
      if (trim($var->value)!='') {
        $output = $var->value;
      }
    }
    return $output;
  }

}
